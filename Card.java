
public class Card {
	
	// 2 properties of a single card in a deck
	private String cardSuit;
	private String cardValue;
	
	// Create a type Card
	public Card(String cardSuit, String cardValue) {
		this.cardSuit = cardSuit;
		this.cardValue = cardValue;
	}
	
	// Setter for Card Suit
	public void setCardSuit(String cardSuit) {
		this.cardSuit = cardSuit;
	}
	
	// Setter for Card Value
	public void getCardValue(String cardValue) {
		this.cardValue = cardValue;
	}
	
	// Getter for Card Suit
	public String getCardSuit() {
		return cardSuit;
	}
	
	//Getter for Card Value
	public String getCardValue() {
		return cardValue;
	}
	
	// Variable to Map Rank to Value
	public int Ace = 1;
	public int Jack = 10;
	public int Queen = 10;
	public int King = 10;
	
	
	
	// Convert String Rank to an Integer Value 
	public int convertRankToInt() {
		if (cardValue == "Ace") {
			return 11;
		}
		if (cardValue == "2") {
			return 2;
		}
		if (cardValue == "3") {
			return 3;
		}
		if (cardValue == "4") {
			return 4;
		}
		if (cardValue == "5") {
			return 5;
		}
		if (cardValue == "6") {
			return 6;
		}
		if (cardValue == "7") {
			return 7;
		}
		if (cardValue == "8") {
			return 8;
		}
		if (cardValue == "9") {
			return 9;
		} else {
			return 10;
		}
	}
	
	// Print Suit and Value as Strings (used for preliminary checking)
	public String toString() {
		return this.cardSuit + this.cardValue;
	}
}
