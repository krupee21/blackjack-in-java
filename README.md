# README #

The 4 files Game.java, Deck.java, Hand.java, and Card.java are the only classes needed for the Blackjack game to be played.


Implements a Blackjack game for one player using user input against a dealer bot. Start with $100 and play $10 games until 
the user loses all their money for that game or chooses to leave. The user has the abilty to hit, stand, double down, split,
and surrender their hands within the rules of Blackjack.