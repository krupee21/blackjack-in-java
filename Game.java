import java.util.*;

public class Game {
	
	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in); 	// User input 
		
		boolean hit = false; 
		boolean currentGame = true;				// These boolean values assist in the control of the game.
		boolean doubleDown = false; 			// For example, if bust is changed to true, the program
		boolean split = false; 					// will know that the player has automatically lost without
		boolean bust = false; 					// even looking at the dealer's hand
		boolean cheating = false;
		boolean roundOver = false; 
		
		double betAmount = 10; 					// These double values assist in currency manipulation.
		double playerMoney = 100; 				// splitHands is used to keep track of the number of hands
		int splitHands = 3;						// when using the SPLIT function 
		
		
		
		
		System.out.println("Welcome to Black Jack!");
		System.out.println();
		System.out.println("Info:");
		System.out.println("- If you want to test the SPLIT functionality please input (4) repeatedly until you ");
		System.out.println("  get a SPLIT case or when the console does NOT say 'Cheating Detected' ");
		System.out.println("  This is a bit tedious, but the occurrence of this is somewhat rare in Blackjack.");
		System.out.println();
		System.out.println("- You start the game with $100 and play till you have no money to play a $10 game.");
		System.out.println("  Once you lose you will be given the option to restart!");
		System.out.println();
		System.out.println("- The program will assume you are inputting valid entries, but will inform you when you make");
		System.out.println("  a decision you can not such as calling SPLIT with two different ranks in your hand");
		System.out.println();
		

		// MAIN GAME LOOP	
		while (currentGame == true) {             
			
			
			
			// This if statement asks the user if they want to keep playing or exit after
			// every hand, and if they make an illegal move it starts a new hand automatically
			
			if (cheating == false) {
				System.out.println("Do you want to start a game/continue playing or exit?");
				System.out.println();
				System.out.println("Enter (0) to PLAY or (1) to EXIT");
				if (s.nextInt() == 1) {
					break;
				}
			}
			
			
			// Resets all boolean values, betAmount, and splitHands(which is used for SPLIT method implementation)
			
			cheating = false;
			roundOver = false;
			hit = false;
			bust = false;
			split = false;
			betAmount = 10;
			splitHands = 3;
			
			
			// Create and Shuffle a Deck for each Round 
			
			Deck deck = new Deck();
			deck.createDeck();
			deck.shuffleDeck();
			
			// Create and populate hand for Player and Dealer
			
			Hand playerHand = new Hand();
			Hand dealerHand = new Hand();
			playerHand.addCard(deck.pickCard());
			playerHand.addCard(deck.pickCard());
			dealerHand.addCard(deck.pickCard());
			dealerHand.addCard(deck.pickCard());
			
			
			
			// Lets the player know their hand's cards and hand's total
			// Shows one of the dealer's cards
			// Asks the user for their next move
			
			System.out.println();
			System.out.println("Your Cards are: ");
			System.out.println(playerHand.toString());
			System.out.println("Your Hand Total is " + playerHand.getHandTotal());
			System.out.println();
			System.out.println("Dealers Cards are: ");
			System.out.println("Face Down and " + dealerHand.dealerToString());
			System.out.println();
			System.out.println("Choose your next move: ");
			System.out.println("(1) for Hit");
			System.out.println("(2) for Stand");
			System.out.println("(3) for Double Down");
			System.out.println("(4) for Split");
			System.out.println("(5) for Surrender");
			int choice = s.nextInt();
			
			
			
			// If SPLIT is Chosen:
			// Make sure that both cards in player's hand are of same rank.
			// Add 2 cards to the player's hand if preliminary cards are same rank.
			// If they are not same rank, then assume they are cheating and
			// restart the round. 
			
			if (choice == 4) {
				split = true;
				splitHands = 2;
				if (playerHand.getCardValue(0) == playerHand.getCardValue(1)) { 
					playerHand.addCard(deck.pickCard());
					playerHand.addCard(deck.pickCard());
				} else { 
					roundOver = true;
					cheating = true;
					System.out.println();
					System.out.println("///////////////////////////////////////////////////////");
					System.out.println("Cheating Detected, New Round");
					System.out.println("///////////////////////////////////////////////////////");
					System.out.println();
				}
				
			}
	
			
			
			// 2 tempHands are created to assist with the functionality of 
			// the SPLIT method which requires multiple hands to be used
			// in a single round of the game
			
			Hand tempHand1 = playerHand;
			Hand tempHand2 = playerHand;
			
			
			
			// Inner Game Loop that allows for multiple split Hands to be used
			// and evaluates all the moves that the user chooses
			
			while (splitHands > 0 && roundOver == false) {
				
				// If the user has chosen SPLIT make playerHand a new hand that  
				// contains one of the original hand's cards and a newly dealt card.
				// Asks the user which move they choose for splitHand1
				
				if (splitHands == 2 && split == true) {
					playerHand = tempHand1.getASplitHand(splitHands);
					System.out.println("Your First Split Hand is: "); 
					System.out.println(playerHand.toString());
					System.out.println("Your First Split Hand Total is " + playerHand.getHandTotal());
					System.out.println();
					System.out.println("Choose your next move: ");
					System.out.println("(1) for Hit");
					System.out.println("(2) for Stand");
					System.out.println("(3) for Double Down");
					System.out.println("(4) for Split");
					System.out.println("(5) for Surrender");
					choice = s.nextInt();								
					splitHands--;
				
				// After first split Hand is played, this initializes a new hand
				// that contains one of the original hand's cards and a newly dealt card.
				// Asks the user which move they choose for splitHand2
				
				} else if (splitHands == 1 && split == true) {			
					playerHand.emptyHand();
					dealerHand.emptyHand();
					dealerHand.addCard(deck.pickCard());
					dealerHand.addCard(deck.pickCard());
					playerHand = tempHand2.getASplitHand(splitHands);
					System.out.println("Your Second Split Hand is: "); 
					System.out.println(playerHand.toString());
					System.out.println("Your Second Split Hand Total is " + playerHand.getHandTotal());
					System.out.println();
					System.out.println("Choose your next move: ");
					System.out.println("(1) for Hit");
					System.out.println("(2) for Stand");
					System.out.println("(3) for Double Down");
					System.out.println("(4) for Split");
					System.out.println("(5) for Surrender");
					choice = s.nextInt();
					splitHands--;
				}
				
				
				
				// If user selects to SURRENDER return half of the money they bet
				// and end Round. If the current Hand is a splitHand then continue in 
				// the Round for the rest of the splitHands
			
				if (choice == 5) {
					playerMoney = playerMoney - betAmount/2;
					if (split == true) {
						System.out.println();
						System.out.println("//////////////////////////////////////////////////////////////////////////////////////////");
						System.out.println("You have surrendered, gotten back $" + betAmount/2 +" and have $" + playerMoney + " left as well as one more hand!");
						System.out.println("//////////////////////////////////////////////////////////////////////////////////////////");
						System.out.println();
					} else {
						roundOver = true;
						System.out.println();
						System.out.println("//////////////////////////////////////////////////////////////////////////////////////////");
						System.out.println("You have surrendered, gotten back $" + betAmount/2 +" and have $" + playerMoney + " left");
						System.out.println("//////////////////////////////////////////////////////////////////////////////////////////");
						System.out.println();
					}
				}
				
				
				// If user selects to DOUBLE DOWN, then double the amount they bet ($10 -> $20).
				// Make doubleDown = true and choice = 1 because this well help us make sure
				// that the next 2 actions are one Hit and then immediately STAND
				
				if (choice == 3) {
					betAmount = 20;
					doubleDown = true;
					choice = 1;
				}
				
				
				
				// If user selects HIT, then they lose the ability to do everything except HIT again or STAND.
				// They have the possibility of busting if they HIT, and if they STAND then the player with
				// the largest total wins the round. 
				
				
				if (choice == 1) {
					hit = true;
					// Every time a user HITs they add a card to their hand.
					// Also lets the user know their hand's cards and hand total
					while (hit == true) { 
						playerHand.addCard(deck.pickCard());
						System.out.println("Your Cards are: ");
						System.out.println(playerHand.toString());
						System.out.println("Your Hand Total is " + playerHand.getHandTotal());
						
						// If the hand total goes over 21 then user has busted
						// and will lose the betAmount
						if (playerHand.getHandTotal() > 21) {
							playerMoney = playerMoney - betAmount;
							//bust = true;
							hit = false;
							
							// Bust, but stay in current Round to play other splitHand
							if (split == true) {
								System.out.println();
								System.out.println("///////////////////////////////////////////////////////");
								System.out.println("Bust, you have lost $" + betAmount +" and have $" + playerMoney + " left");
								System.out.println("///////////////////////////////////////////////////////");
								System.out.println();
								hit = false;
								continue;
							
							// Bust and end current Round
							} else {
								roundOver = true;
								System.out.println();
								System.out.println("///////////////////////////////////////////////////////");
								System.out.println("Bust, you have lost $" + betAmount +" and have $" + playerMoney + " left");
								System.out.println("///////////////////////////////////////////////////////");
								System.out.println();
								hit = false;
								continue;
							}
						}
						
						
						// If user selected DOUBLE DOWN and hasn't busted after their HIT prepare to STAND
						if (doubleDown == true && playerHand.getHandTotal() < 22) {
							choice = 2;
							hit = false;
							
						}
						
						// If user didn't DOUBLE DOWN then ask if they want to continue to HIT or simply STAND
						if (doubleDown == false && playerHand.getHandTotal() < 22) {
							System.out.println("Choose your next move: ");
							System.out.println("(1) for Hit");
							System.out.println("(2) for Stand");
							choice = s.nextInt();
							if (choice == 1) {
								hit = true;
							} else {
								choice = 2;
								hit = false;
							}
						}
					}
					
					// Check to see if player has no more money, reset their moeny in case they choose to play again
					
					if (playerMoney < betAmount) {
						System.out.println();
						System.out.println("///////////////////////////////////////////////////////");
						System.out.println("You have lost this round, but will be given $100 if you choose to restart");
						System.out.println("///////////////////////////////////////////////////////");
						System.out.println();
						playerMoney = 100;
						break;
					}
					
				}
				
				
				// If user selects STAND and they haven't busted already make sure dealer has
				// a hand total of over 16 by adding cards to their hand till they reach a total
				// over 16. Notify user of dealer's cards and dealer's total. 
				
				if (choice == 2) {
					while (dealerHand.getHandTotal() < 17) {
						dealerHand.addCard(deck.pickCard());
					}
					
					System.out.println("Dealers Cards were: ");
					System.out.println(dealerHand.toString());
					System.out.println("Dealers Hand Total was " + dealerHand.getHandTotal());
					
					System.out.println();
					
					// If dealer busts then allocate winnings to player and end current Round.
					// If current hand is a splitHand then stay in current Round
					// to evaluate other splitHands if needed
					
					if (dealerHand.getHandTotal() > 21) {
						playerMoney = playerMoney + betAmount;
						if (split == true) {
							System.out.println();
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println("Dealer busted, you have won $" + betAmount + " and have $" + playerMoney);
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println();
							continue;
						} else {
							roundOver = true;
							System.out.println();
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println("Dealer busted, you have won $" + betAmount + " and have $" + playerMoney + " left");
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println();
							continue;
						}
					}
					
					// If neither play busts we are at a stand off and the player with
					// the larger hand total wins. If we are dealing with a splitHand then
					// stay in current Round to evaluate other splitHands if needed.
					
					int dealerTotal = dealerHand.getHandTotal();
					int playerTotal = playerHand.getHandTotal();
					
					// If player has larger hand total
					
					if (dealerTotal > playerTotal) {
						playerMoney = playerMoney - betAmount;
						if (split == true) {
							System.out.println();
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println("You have lost $" + betAmount + ", you have $" + playerMoney + " left");
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println();
						} else {
							roundOver = true;
							System.out.println();
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println("You have lost $" + betAmount + ", you have $" + playerMoney + " left");
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println();
						}
						
						// Another check to see if user has more money to play with 
						
						if (playerMoney < betAmount) {
							System.out.println();
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println("You have lost this round, but will be given $100 if you choose to restart");
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println();
							playerMoney = 100;
							break;
						}
					}
					
					// If dealer has larger hand total
					
					if (dealerTotal < playerTotal) {
						playerMoney = playerMoney + betAmount;
						if (split == true) {
							System.out.println();
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println("You have won $" + betAmount + ", you have $" + playerMoney + " left");
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println();
						} else {
							roundOver = true;
							System.out.println();
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println("You have won $" + betAmount + ", you have $" + playerMoney + " left");
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println();
						}
					}
					
					// If dealer and player have same hand total then push/tie has occurred
					
					if (dealerTotal == playerTotal) {
						if (split == true) {
							System.out.println();
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println("Push/Tie has occurred");
							System.out.println("You still have $" + playerMoney);
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println();
						} else {
							roundOver = true;
							System.out.println();
							System.out.println("///////////////////////////////////////////////////////");
							System.out.println("Push/Tie has occurred");
							System.out.println("You still have $" + playerMoney);
							System.out.println("///////////////////////////////////////////////////////");
								System.out.println();
						}
					}
				}			
			}	
		}
	}
}
