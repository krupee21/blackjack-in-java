import java.util.*;

public class Deck {
	
	// Deck of type Card
	private ArrayList<Card> Deck;
	
	// Array to hold all Suits and Array to hold all Ranks
	
	String[] suitsArray = new String[] {"Hearts", "Spades", "Clubs", "Diamonds"};
	String[] rankArray = new String[] {"Ace", "2", "3", "4" ,"5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"}; 
	
	// Number of Cards = 52, but 0-51 indices
	private int numCards = 51;
	
	// Takes Suit and Value Array and populates Deck ArrayList with Cards using a double for loop that gives each suit 13 different values
	public void createDeck() { 
		Deck = new ArrayList<Card>();
		int numCards = 0; // 51 due to indices
		for (int i = 0; i < suitsArray.length; i++) {
			for (int j = 0; j < rankArray.length; j++) {
				this.Deck.add(new Card(suitsArray[i], rankArray[j]));
				numCards = numCards + 1;
			}
		}
	}
	
	// Use Collections() to shuffle all the contents in the ArrayList randomly to mimic a real shuffled Deck
	public void shuffleDeck() {
		Collections.shuffle(Deck);
	}
	
	// Remove a card from the top of the Deck, used when dealing cards to Hands
	public Card pickCard() {
		Card picked = Deck.get(numCards);
		Deck.remove(numCards);
		numCards = numCards - 1;
		return picked;
	}
	
	// Prints all Cards in Deck (used for preliminary checking of Deck contents)
	public String toString() {
		String str = "";
		for (int i = 0; i < Deck.size(); i++) {
			str = Deck.get(i).getCardSuit();
			str = str + " " + Deck.get(i).getCardValue();
			System.out.println(str);
		}
		return "";
	}
	
	
	


}



