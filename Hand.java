import java.util.ArrayList;

public class Hand {
	
	// Holds cards, acts as a physical hand that a player/dealer would hold
	private ArrayList<Card> hand = new ArrayList<Card>();

	
	// Add a particular card to the Hand 
	public void addCard(Card card) {
		hand.add(card);
		
	}
	
	// Get rid of all cards in the Hand
	public void emptyHand() {
		for (int i = hand.size() - 1; i > 0; i--) {
			hand.remove(i);
		}
	}
	
	// Get number of cards in the Hand
	public int getHandSize() {
		return hand.size();
	}
	
	// For SPLIT, takes you hand and creates a new Hand that has one card from the old Hand and one newly drawn Card
	public Hand getASplitHand(int index) {
		Hand splitHand = new Hand();
		if (index == 2) {
			for (int i = 0; i < hand.size(); i = i + 2) {
				splitHand.addCard(hand.get(i));
			}
		}
		if (index == 1) {
			for (int i = 1; i < hand.size(); i = i + 2) {
				splitHand.addCard(hand.get(i));
			}
		}
		return splitHand;
	}
	
	
	// Print method to show all Cards of a Hand by printing Suit and Rank
	public String toString() {
		String str = "";
		for (int i = 0; i < hand.size(); i++) {
			if (i == hand.size() - 1) {
				str = str + hand.get(i).getCardValue() + " of " + hand.get(i).getCardSuit();	
			} else {
				str = str + hand.get(i).getCardValue() + " of " + hand.get(i).getCardSuit() + ", ";
			}
		}
		return str;
	}
	
	// Print method to show one Card of a dealer's Hand and keep the other hidden
	public String dealerToString() {
		String str = "";
		str = this.hand.get(1).getCardValue() + " of " + this.hand.get(1).getCardSuit();
		return str;
	}
	
	// Get the value/rank of a card at a specific location in the hand
	public String getCardValue(int index) {
		return hand.get(index).getCardValue();
	}
	
	
	// Get the sum of all ranks/values in a given Hand
	public int getHandTotal() {
		int total = 0;
		for (int i = 0; i < hand.size(); i++) {
			total = total + hand.get(i).convertRankToInt();
			if (hand.size() > 1) {
				if (hand.get(i).getCardValue() == "Ace") {
					if (total > 21) {
						total = total - 10;
					}
				}
			}
		}
		return total;
	}
}
